import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Form {
	protected WebDriver driver; 
	
	//Locators to find elements
	private By firstName = By.id("first-name");
	private By lastName = By.id("last-name");
	private By job = By.id("job-title");
	private By education = By.id("radio-button-2");
	private By sex = By.id("checkbox-2");
	private By yearsOfExp = By.cssSelector("option[value='1']");
	private By date = By.id("datepicker");
	private By today = By.className("today");
	
	public Form(WebDriver driver) { //Constructor method
		this.driver= driver;
	}
	public void fill(WebDriver driver) {
		driver.findElement(firstName).sendKeys("Karla Julieta");
		driver.findElement(lastName).sendKeys("Jim�nez Casta�eda");
		driver.findElement(job).sendKeys("IT Trainee");
		driver.findElement(education).click();
		driver.findElement(sex).click();
		driver.findElement(yearsOfExp).click();
		driver.findElement(date).click();
		driver.findElement(today).click();
	}
}
