import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Form {
	protected WebDriver driver; 
	
	//Locators to find elements
	private By firstName = By.id("first-name");
	private By lastName = By.id("last-name");
	private By jobBy = By.id("job-title");
	private By education;
	private By sex;
	private By yearsOfExp;
	private By dateBy= By.id("datepicker");
	
	String fname;
	String lname;
	String job;
	String date;
	
	//CREATE DIRECTORY FOR SCREENSHOTS
	Date currentdate= new Date();
	String ssDirForm1 = ".//screenshots/testForm1-" + 
			currentdate.toString().replace(" ", "-").replace(":", "-")
			+ ".png"; 
	String ssDirForm2= ssDirForm1.replace("Form1", "Form2");
	String ssDirForm3= ssDirForm1.replace("Form1", "Form3");
	
	public Form(WebDriver driver, String fname, String lname, String job, String educationId,
		    String sId, String experienceId, String date) { //Constructor method
		this.driver= driver;
		this.fname= fname;
		this.lname= lname;
		this.job= job;
		education = By.id(educationId);
		sex = By.id(sId);
		yearsOfExp= By.cssSelector(experienceId);
		this.date= date;
	}
	public void fill() throws IOException {
		driver.findElement(firstName).sendKeys(fname);
		driver.findElement(lastName).sendKeys(lname);
		driver.findElement(jobBy).sendKeys(job);
		driver.findElement(education).click();
		driver.findElement(sex).click();
		//CHECKBOX SCREENSHOT
		File ssFile =((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssFile, new File(ssDirForm1));
		
		driver.findElement(yearsOfExp).click();
		//SELECT SCREENSHOT
		ssFile =((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssFile, new File(ssDirForm2));
		
		driver.findElement(dateBy).sendKeys(date);
		driver.findElement(dateBy).sendKeys(Keys.ENTER);
		//DATEPICKER SCREENSHOT
		ssFile =((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssFile, new File(ssDirForm3));
	}
}
