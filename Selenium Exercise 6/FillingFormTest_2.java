import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@RunWith(value = Parameterized.class)
public class FillingFormTest {
	WebDriver driver;
	String ssDirSub;
	
    String fname; 
    String lname;
    String job;
    String edId;
    String sexId;
    String expId;
    String date;

	    public FillingFormTest(String fname, String lname, String job, String educationId,
	    String sId, String experienceId, String date) {
	        this.fname= fname;
	        this.lname= lname;
	        this.job= job;
	        switch(educationId) {
	        case "HS":
	        	edId= "radio-button-1"; break;
	        case "C":
	        	edId= "radio-button-2"; break;
	        case "G":
	        	edId= "radio-button-3"; break;
	        }
	        
	        switch(sId) {
	        	case "M":
	        		sexId= "checkbox-1"; break;
	        	case "F":
	        		sexId= "checkbox-2"; break;
	        	default:
	        		sexId= "checkbox-3"; break;
	        }
	        
	        int years= Integer.parseInt(experienceId);
	        if(years>10) 
	        	expId= "option[value='4']";
	        else if (years>= 5 && years<10)
	        	expId= "option[value='3']";
	        else if (years>= 2 && years<5)
	        	expId= "option[value='3']";
	        else
	        	expId= "option[value='3']";
	        this.date= date;
	        	
	        } 
	    @Parameters
	    public static Collection<Object[]> formData() {
	    	String path= "C:/Users/2103410/Downloads/FormyData.xlsx";
			ExcelReader reader = new ExcelReader(path);
			
			Object[] set1= reader.readRow(1).toArray();
			Object[] set2= reader.readRow(2).toArray();
			
			Object[][] formydata= {set1, set2};
	        return Arrays.asList(formydata);
	       }
	
		@Before
		public void beforeTest() {
			System.setProperty("webdriver.chrome.driver", 
		   "C:/Users/2103410/Documents/ILP Training/Herbal Life/Automation Testing Workshop/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://formy-project.herokuapp.com/form");
			Date currentdate= new Date();
			ssDirSub = ".//screenshots/testSubmission-" + 
					currentdate.toString().replace(" ", "-").replace(":", "-")
					+ ".png"; 
		}    
	    
	@Test
	public void testSubmissionText() throws IOException {
		Form form= new Form(driver,fname, lname, job, edId, sexId, expId, date); //Page Object Model
		form.fill();
		
		driver.findElement(By.cssSelector("a[href='/thanks']")).click(); // Submit form
		
		WebDriverWait w= new WebDriverWait(driver, Duration.ofSeconds(5)); //Adding explicit wait
		w.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-success")));
		
		String txt = driver.findElement(By.tagName("h1")).getText();
		String expectedOutput= "Thanks for submitting your form";
		
		//SUBMISSION SCREENSHOT
		File ssFile =((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssFile, new File(ssDirSub));
		
		assertEquals(txt, expectedOutput); //Assertion	
		}
	
	@After
	public void afterTest() {
		driver.quit();
	}
}
