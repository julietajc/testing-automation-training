import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	String path;
	FileInputStream fis;
	XSSFWorkbook wb; 
	XSSFSheet sheet;
	DataFormatter formatter = new DataFormatter();
	
	public ExcelReader(String path) {
		this.path=path;
		try {
			fis =new FileInputStream(path);
			wb =new XSSFWorkbook(fis);
			sheet= wb.getSheetAt(0);}
		catch (Exception e) {
			e.printStackTrace();}}
	
	public List <String> readRow(int n) {
		    List <String> rowValues = new ArrayList<String>();
		    Row row = sheet.getRow(n);
		    for(Cell cell: row) {
				//System.out.println(formatter.formatCellValue(cell));
		    	rowValues.add(formatter.formatCellValue(cell));	}	
		    return rowValues;}
	
	public List <String> readColumn(int n) {
		List <String> colValues = new ArrayList<String>();
		int nRows = sheet.getLastRowNum();
		for(int i=0; i<=nRows; i++) {
			Cell cell= sheet.getRow(i).getCell(n);
			//System.out.println(formatter.formatCellValue(cell));
			colValues.add(formatter.formatCellValue(cell)); }
	    return colValues; }
}
