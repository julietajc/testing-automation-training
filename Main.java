
public class Main {

	public static void main(String[] args) {
		//Assignment 1.1
		String Str1= "hot";
		String Str2= "dog";
		System.out.println(Str1+Str2);
		
		//Assignment 1.2
		int num=getGreatestNum(23, 12,19);
		System.out.println( "The greatest number is " + num);
        num=getGreatestNum(122,98,377); 
		System.out.println( "The greatest number is " + num);

		//Assignment 1.3
		int[] fib= printFibonacciNum(8);
		for(int i=0; i<fib.length; i++) {
			System.out.print(fib[i]+", ");
		}
		
		fib= printFibonacciNum(25);
		System.out.println(" ");
		for(int i=0; i<fib.length; i++) {
			System.out.print(fib[i]+", ");
		}
		System.out.println(" ");
        //Assignment 1.4
		System.out.println(getGreatestNum(13, 199, -2)); //Output using first method
		System.out.println(getGreatestNum(13, 199, -2, 45)); //Output using second method

	}
	
	public static int getGreatestNum(int n1, int n2, int n3) {
		if ( n1 > n2 && n1 > n3 )
			return n1;
	    else

	        if ( n2 > n3 )
	        	return n2;
	        else
	        	return n3;
	}

	public static int[] printFibonacciNum(int num) {
		int[] fibSeries = new int[num];
		fibSeries[0]=1;
		fibSeries[1]=2;
		for(int i=2; i<num; i++) {
			fibSeries[i]= fibSeries[i-1] +fibSeries[i-2];
		}
		return fibSeries;
	}

	//Overloaded method
	public static int getGreatestNum(int n1, int n2, int n3, int n4) {
		if ( n1 > n2 && n1 > n3 && n1 > n4)
			return n1;
	    else if ( n2 > n3 && n2 > n4)
	        	return n2;
	        else if (n3 > n4)
	        	return n3;
	        else
	        	return n4;
	}
}
