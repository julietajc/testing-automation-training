import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Form {
	protected WebDriver driver; 
	
	//Locators to find elements
	private By firstName = By.id("first-name");
	private By lastName = By.id("last-name");
	private By jobBy = By.id("job-title");
	private By education;
	private By sex;
	private By yearsOfExp;
	private By dateBy= By.id("datepicker");
	
	String fname;
	String lname;
	String job;
	String date;
	
	public Form(WebDriver driver, String fname, String lname, String job, String educationId,
		    String sId, String experienceId, String date) { //Constructor method
		this.driver= driver;
		this.fname= fname;
		this.lname= lname;
		this.job= job;
		education = By.id(educationId);
		sex = By.id(sId);
		yearsOfExp= By.cssSelector(experienceId);
		this.date= date;
	}
	public void fill() {
		driver.findElement(firstName).sendKeys(fname);
		driver.findElement(lastName).sendKeys(lname);
		driver.findElement(jobBy).sendKeys(job);
		driver.findElement(education).click();
		driver.findElement(sex).click();
		driver.findElement(yearsOfExp).click();
		driver.findElement(dateBy).sendKeys(date);
	}
}
