import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FillingForm {
	public static void main(String[] args){
		System.setProperty("webdriver.chrome.driver", 
	   "C:/Users/2103410/Documents/ILP Training/Herbal Life/Automation Testing Workshop/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		
		driver.findElement(By.id("first-name")).sendKeys("Karla Julieta");
		driver.findElement(By.id("last-name")).sendKeys("Jiménez Castañeda");
		driver.findElement(By.id("job-title")).sendKeys("IT Trainee");
		driver.findElement(By.id("radio-button-2")).click();
		driver.findElement(By.id("checkbox-2")).click();
		driver.findElement(By.cssSelector("option[value='1']")).click();
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		driver.findElement(By.cssSelector("a[href='/thanks']")).click();
		driver.quit();
		}
}
