import java.util.Random;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Student {
	int studentId;
	String firstName;
	String middleName;
	String lastName;
	LocalDate DOB;
	
	public Student(String firstName, String middleName,
	               String lastName, String date){
		Random rand = new Random(); 
		this.studentId= rand.nextInt(100)+1; //Sets a random number from 1 to 100
		
		this.firstName= firstName;
		this.middleName= middleName;
		this.lastName= lastName;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		this.DOB= LocalDate.parse(date, formatter);
	}
	public int getID() {
		return studentId;
	}
	
	public String concatName() {
		String concat= firstName + " " + middleName + " " + lastName;
		return concat;
	}
    
	public String displayFormatedDate() {
		String formattedDate = DOB.format(DateTimeFormatter.ofPattern("yyyy-dd-MM"));
		return formattedDate;
	}
}
