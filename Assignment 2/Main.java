//Java Programming Exercise 2
public class Main {
	public static void main(String[] args) {
	//Assignment 2.1
		displayFib(8);
		System.out.println("");
		
     //Assignment 2.2
		divideByZero(4,0);
		concatString("TCS", null);
		
	//Assignment 2.3
		Student s1 = new Student("Karla", "Julieta", "Jim�nez", "09-01-1998");
		Student s2 = new Student("Jos�", "Manuel", "Ortega", "21-09-1997");
		Student s3 = new Student("Mar�a", "Jos�", "Hernandez", "15-12-1998");
		
		System.out.println(s1.getID());
		System.out.println(s2.getID());
		System.out.println(s3.getID());
		
		System.out.println(s1.concatName());
		System.out.println(s2.concatName());
		System.out.println(s3.concatName());
		
		System.out.println(s1.displayFormatedDate());
		System.out.println(s2.displayFormatedDate());
		System.out.println(s3.displayFormatedDate());
	}
	
	public static int fibNum(int n){
		if(n == 1 || n== 2)
            return n;
       else
            return fibNum(n-1) + fibNum(n-2);
	}
	
	public static void displayFib(int n) {
		for (int i=1; i<=n; i++) {
			System.out.print(fibNum(i) + ", ");
		}
	}
    
	public static void divideByZero(int dividend, int divisor){
		try {
			double quotient= dividend/divisor;
			System.out.println(dividend+ "/" + divisor + "=" + quotient);
		}
		catch(ArithmeticException e){
			System.out.println("Error: " + e.getMessage());
		}
	} 

	public static void concatString(String str1, String str2){
		try {
			String concat= str1.concat(str2);
			System.out.println(concat);
		}
		catch(Exception e){
			System.out.println("Error: " + e.getMessage());
		}
	} 
}